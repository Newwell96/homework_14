import gameStore from "../store/GameStore";
class Broker {
    constructor() {
        this.listeners = {};
    }
    emit(type, payload) {
        if (this.listeners[type]) {
            this.listeners[type].forEach((cb) => cb(type, payload, this));
        }
    }
    on(type, cb) {
        if (!this.listeners[type]) {
            this.listeners[type] = [];
        }
        this.listeners[type].push(cb);
    }
}

class gameWS {

    socket;

    constructor() {

        this.socket = new WebSocket('ws://localhost:9000');

        this.socket.addEventListener("open", (event) => {
            console.log("connected to WS Server");
        })

        this.socket.addEventListener("message", (event) => {
            const data = JSON.parse(event.data);
            const message = data.message ? console.log(data.message, "--Ответ сервера--") : null;
            const field = data.field ? localStorage.setItem('currentField', JSON.stringify(data.field)) : null;
            gameStore.initField();
        });

        this.broker = new Broker();
        this.broker.on('send-message', this.onMessageSent.bind(this));
        this.broker.on('make-move', this._handleMakeMove.bind(this));
        this.broker.on('move-made', this.onMoveMade.bind(this));
        this.broker.on('move-failed', this.onMoveFailed.bind(this));
        this.broker.on('error', this.onError.bind(this));
    }

    sendData(field) {
        try {
            this.socket.send(JSON.stringify({field, action: "field" }));
        }
        catch (error) {
            this._handleError("Произошла ошибка отправки сообщения.")
        }
    }

    async sendMessage(userId, text) {
        try {
            this.socket.send(JSON.stringify({userId, text}));
        }
        catch (error) {
            this._handleError("Произошла ошибка отправки сообщения.")
        }
    }

    onMessageSent(type, payload) {
        console.log(`${payload.userId} отправил сообщение: ${payload.text}`,"--Ответ сервера--");
        // this.broker.emit('message-sent', payload);
    }

    async makeMove(userId, row, col, cell,field) {
        console.log(`Игрок ${userId} сделал ход ${row}:${col}`,"--Сообщение серверу--");

        try {
            this.socket.send(JSON.stringify({userId, row, col, cell, field, action: "move" }));
        }
        catch (error) {
            this._handleError(error, "Ошибка хода.")
        }
    }

    onMoveMade(type, payload) {
        console.log(`Игрок ${payload.userId} сделал ход ${payload.row}:${payload.col}`,"--Ответ сервера--");
    }

    onMoveFailed(type, payload) {
        console.log("Ход невозможен","--Ответ сервера--" );
    }

    onError(type, payload) {
        console.error(payload.error, payload.reason,"--Ответ сервера--" );
    }

    _handleMakeMove(type, payload) {

        if (!payload.cell) {
            this.broker.emit('move-made', {userId: payload.userId, row: payload.row, col: payload.col});
        } else {
            this.broker.emit('move-failed', {});
        }
    }

    _handleError(error, reason) {
        this.broker.emit('error', {error, reason});
    }

}

const gameWebSocket = new gameWS();
export default gameWebSocket;