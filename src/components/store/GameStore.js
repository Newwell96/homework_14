import {action, autorun, computed, makeObservable, observable, reaction} from 'mobx';
import MyCell from "../pages/gamePanel/gameBoard/components/cell/cell";
import React, {createContext} from "react";
import gameAPI from "../API_WS/gameAPI";
import gameWebSocket from "../API_WS/webSocket";

class GameStore {

    users = [];
    chat = [];
    timeStart = null;
    squares = Array(9).fill(null);
    xIsNext = true;
    winner = null;
    winnerName = null;
    isGameRunning = true;
    winnerCombination = [];
    modalActive = false;
    seconds = 0;
    intervalId = null;
    role = null;

    constructor() {

        makeObservable(this, {
            squares: observable,
            setSquares: action,

            role: observable,
            setRole: action,

            chat: observable,
            setChat: action,

            xIsNext: observable,
            setXIsNext: action,

            isGameRunning: observable,
            setIsGameRunning: action,

            winner: observable,
            setWinner: action,

            winnerName: observable,
            setWinnerName: action,
            calculateWinnerName: action,

            winnerCombination: observable,
            setWinnerCombination: action,

            modalActive: observable,
            setModalActive: action,

            seconds: observable,
            intervalId: observable,
            timeStart: observable,

            formattedTime: computed,
            startTimer: action,
            stopTimer: action,
            resetTimer: action,

            playerO: computed,
            playerX: computed,
            users: observable,

        })

        autorun((startTimer) => {
            this.startTimer();
        });

        reaction(() => this.winner, () => {
            if (this.winner) {
                this.stopTimer();
            }
            this.calculateWinnerName();
        });

        reaction(() => this.squares, () => {
            this.calculateWinner(this.squares);
            if (this.squares.includes(null)) {
            } else {
                this.calculateWinnerName();
                this.setIsGameRunning(false);
                this.setModalActive(true);
            }
        });

        reaction(() => this.chat, () => {
            gameAPI.sendChatMessage(this.chat)
        });

        this.initInfo();
        this.initField();
        this.initChat();

    }

    get playerO() {
        return this.users["O"];
    }

    get playerX() {
        return this.users["X"];
    }

    get nameX() {
        return this.playerX?.surName + " " + this.playerX?.firstName;
    }

    get nameO() {
        return this.playerO?.surName + " " + this.playerO?.firstName;
    }

    get formattedTime() {
        const minutes = Math.floor(this.seconds / 60);
        const remainingSeconds = this.seconds % 60;
        const formattedTime = `${minutes < 10 ? '0' : ''}${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`;
        return formattedTime;
    }

     async initInfo() {
        const playersInfo = await gameAPI.getPlayersInfo();
        this.setUsers(playersInfo);
        console.log(playersInfo)
        this.initRole()
        this.setStartTimer( playersInfo.timeStart)
     }

    initRole() {
        const playerID = JSON.parse(localStorage.getItem('userData')).idUser
        if (playerID === this.playerO?.id) {
            this.setRole("O")
        }
        else if (playerID === this.playerX?.id) {
            this.setRole("X")
        }
    }

    setRole (newRole) {
    this.role = newRole;
}
     initChat() {
        const chat = JSON.parse( gameAPI.getChatMessage());
        if (chat) {
            this.setChat(chat);
        }
    }

    shouldSkipTurn(role, xIsNext) {
        return (role === "X" && !xIsNext) || (role === "O" && xIsNext) || (!role);
    }

    initNext(squares) {

        const numX = squares.filter(sq => sq === 'X').length;
        const numO = squares.filter(sq => sq === 'O').length;

        if (numX === numO) {
            this.setXIsNext(true);
        } else if (numO > numX) {
            this.setXIsNext(true);
        } else {
            this.setXIsNext(false);
        }
    }

    async initField() {
        const squaresFromStorage = JSON.parse( gameAPI.getFieldState());
        if (squaresFromStorage) {
            this.setSquares(squaresFromStorage);
        }
        this.initNext(this.squares);
    }


    setCurrentField(value) {
        this.currentField = value;
    }

    setChat(newChat) {
        this.chat = newChat;
    }

    setUsers(users) {
        this.users = users;
    }

    setStartTimer(date) {
        if (!date) {
            this.timeStart = new Date();
            gameAPI.sendTimeStart(this.timeStart);
        } else {
            this.timeStart = new Date(date);
            this.seconds = this.getSecondsBetweenDates(this.timeStart, new Date());
        }
    }

    calculateWinnerName() {
        if (this.playerX.side === this.winner) {
            this.setWinnerName(this.nameX)
        } else if (this.playerO.side === this.winner) {
            this.setWinnerName(this.nameO)
        } else this.setWinnerName("Сопернический дух")
    }

    setSquares(newSquares) {
        this.squares = newSquares;
    };

    setXIsNext(value) {
        this.xIsNext = value;
    };

    setIsGameRunning(isRunning) {
        this.isGameRunning = isRunning;
    };

    setWinner(winner) {
        this.winner = winner;
        this.calculateWinnerName();
    };

    setWinnerName(name) {
        this.winnerName = name;
    };

    setWinnerCombination(comb) {
        this.winnerCombination = comb;
    };

    setModalActive(value) {
        this.modalActive = value;
    };

    setSeconds(newSeconds) {
        this.seconds = newSeconds
    };

    handleClick(i) {
        if (this.shouldSkipTurn(this.role, this.xIsNext)) return;
        const row = Math.floor(i / 3);
        const col = i % 3;
        if (this.winner || this.squares[i]) return;
        const newSquares = this.squares.slice();
        newSquares[i] = this.xIsNext ? 'X' : 'O';
        gameWebSocket.makeMove(this.xIsNext ? this.nameX : this.nameO, row, col, this.squares[i],newSquares)
        this.setSquares(newSquares);
        this.setXIsNext(!this.xIsNext);
        this.calculateWinner(newSquares);
        this.handleStopTimer();
    }

    calculateWinner(squares) {
        const lines = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6],];
        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
                console.log("Есть победитель");
                this.setWinnerCombination(lines[i]);
                this.setWinner(squares[a]);
                this.setModalActive(true);
                return;
            }
        }
    }

    handleStopTimer = () => {
        this.setIsGameRunning(false);
    };

    renderSquare(i) {
        let className = "cell";
        if (this.winnerCombination.includes(i)) {
            if (this.winner === "X") {
                className = "cell winX"
            } else if (this.winner === "O") {
                className = "cell winO"
            }
        }
        return (<MyCell
                className={className}
                value={this.squares[i]}
                onClick={() => this.handleClick(i)}
            />);
    }

    getSecondsBetweenDates(date1, date2) {
        const diffInMilliseconds = Math.abs(date2 - date1);
        return Math.floor(diffInMilliseconds / 1000);
    }

    startTimer() {
        if (this.isGameRunning) {
            this.intervalId = setInterval(() => {
                this.seconds += 1;
            }, 1000)
        }
    }

    stopTimer() {
        this.isGameRunning = false;
        clearInterval(this.intervalId);
    }

    resetTimer() {
        this.seconds = 0;
        this.isGameRunning = false;
        clearInterval(this.intervalId);
    }
}
const gameStore = new GameStore();
export default gameStore;
export const GameContext = createContext();
