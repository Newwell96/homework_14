import React, {useState} from 'react';
import MyButton from "../../UI components/button/button";
import MyInput from "../../UI components/input/input";
import MyImage from "../../UI components/image/image";
import './auth.css'


function Authorization ({setIsAuthorized}) {
    const [isErrorAuth, setIsErrorAuth] = useState(false);
    console.log(isErrorAuth, "isError")

//Отправляем данные API_WS
    const loginFunc = async (login, password) => {

        const accessLogin = document.getElementById("login").value;
        const accessPassword = document.getElementById("password").value;

        const response = await fetch('/auth', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify({login:accessLogin,
                password:accessPassword})
        });

//Обработка ответа API_WS
        if (response.ok) {
            const data = await response.json();
            localStorage.setItem('userData', JSON.stringify(data));
            setIsAuthorized(true);
        } else {
            setIsErrorAuth(true)
        }
    };

//Валидация
    const inputHandler = (event) => {
        const loginInput = document.getElementById("login");
        const passwordInput = document.getElementById("password");
        const submitInput = document.getElementById("submit");

        event.target.value = event.target.value.replace(/[^a-zA-Z0-9._]/g, "");

        if (loginInput.value !== "" && passwordInput.value !== "") {
            submitInput.style.backgroundColor = "";
        }
        else {
            submitInput.style.backgroundColor = "rgba(96, 194, 170, 0.3)";
        }
    }

    //TODO Не работает - Запрет копирования

    // function handleCopy(event) {
    //     event.preventDefault();
    //     event.nativeEvent.stopImmediatePropagation();
    // }
    // useRestrictCopyPaste({window, actions:["copy"]})

    function checkAuth(isErrorAuth, inputId) {

        const inputElement = document.getElementById(inputId);
        let errMsg = 'Неверные данные';

        if (isErrorAuth) {
            if (inputId === 'login') {
                errMsg = "Неверный логин";
            } else if (inputId === 'password') {
                errMsg = "Неверный пароль";
            }

            return {
                error: errMsg,
                style: {
                    border: "1px solid #e93e3e"
                },
                errorStyle: "error_text",
            };
        }
    }

    return (
        <div className="auth_body">
        <div id="authorization_container">
            <MyImage
                name="dog"
            />
            <div id="text" align="center">Войдите в игру</div>
            <div className="data">
                {
                    <React.Fragment>
                        <MyInput
                            className="text-field__input "
                            id="login"
                            name="login"
                            type="text"
                            placeholder="Логин"
                            onInput={inputHandler}
                            {...checkAuth(isErrorAuth, 'login')}
                        />
                        {/*Логин: Nikita*/}
                        <MyInput
                            className="text-field__input "
                            id="password"
                            name="password"
                            type="password"
                            placeholder="Пароль"
                            onInput={inputHandler}
                            {...checkAuth(isErrorAuth, 'password')}
                        />
                        {/*Пароль: Pomytkin*/}
                    </React.Fragment>
                }
                <MyButton
                    className="button background-default-green"
                    buttonText="Войти"
                    onClickFunction={loginFunc}
                    buttonID={"submit"}
                />
                </div>
        </div>
        </div>
    )
}

export default Authorization;
