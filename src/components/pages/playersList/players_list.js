import React, {useState} from 'react';
import MyButton from "../../UI components/button/button";
import "../../../index.css"
import "./components/playerListRow/playerListRow.css"
import playerListData from "./data/playerListData"
import PlayerListRow from "./components/playerListRow/playerListRow";
import MyModalAdd from "./components/modalPlayerList/modalPlayerList";

function PlayerList() {

    const [stateList, setStateList] = useState(playerListData["playerListData"]);
    const [modalActive, setModalActive] = useState(false);

    function changePlayerStatus(index) {
        const updatedActiveList = [...stateList];
        updatedActiveList[index].status = !updatedActiveList[index].status;
        setStateList(updatedActiveList);
    }

    function renderRows(data) {
        return data.map( (item,index) => {
                    return (
                        <PlayerListRow
                            name={item["name"]}
                            age={item["age"]}
                            gender={item["gender"]}
                            status={item["status"]}
                            creatingDate={item["creatingDate"]}
                            changedDate={item["changedDate"]}
                            onChangeStatus = {
                                () => {changePlayerStatus(index);}
                            }
                            stateList={stateList}
                            setStateList={setStateList}
                        />
                    );
        });
    }

    return (<div className="tables_body">
            <div id="tables_container">
                <div className="headTable">
                    <h1 className="scores_title">Список игроков</h1>
                    <MyButton
                        className="button background-add-player"
                        buttonText="Добавить игрока"
                        onClickFunction={
                            () => {setModalActive(true)}
                        }
                    />
                </div>

                <table className="table">
                    <thead>
                    <tr className="player-list_row">
                        <th className="first_colomn">
                           ФИО
                        </th>
                        <th className="age_colomn">
                            Возраст
                        </th>
                        <th className="gender_colomn">
                            Пол
                        </th>
                        <th className="status_colomn">
                            Статус
                        </th>
                        <th className="tipic_colomn">
                            Создан
                        </th>
                        <th className="tipic_colomn">
                            Изменен
                        </th>
                        <th className="last_colomn">
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    {renderRows(stateList)}

                    </tbody>
                </table>
            </div>
            <MyModalAdd
                stateList={stateList}
                setStateList={setStateList}
                active={modalActive}
                setActive={
                ()=> {
                setModalActive()
                }
            }
            />
        </div>

    )
}


export default PlayerList;