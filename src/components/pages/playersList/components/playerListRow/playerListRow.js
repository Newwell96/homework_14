import React, {useState} from 'react';
import MyImage from "../../../../UI components/image/image";
import Chip from "../../../../UI components/chip/chip";
import MyButton from "../../../../UI components/button/button";
import "./playerListRow.css"

const PlayerListRow = (props) => {

    const [stateStatus, setStateStatus] = useState(props.status);

    let imageName;
    if (props.gender === "male") {
        imageName = "boy"
    } else {
        imageName = "girl"
    }

    let playerStatus;
    let statusText;
    let statusAction;

    if (props.status) {
        playerStatus = "chip player-status background-light-green"
        statusText = "Активен"
        statusAction = "Заблокировать"
    } else {
        playerStatus = "chip player-status background-light-red"
        statusText = "Заблокирован"
        statusAction = "Разблокировать"
    }

    const setBlock = () => {
        if (stateStatus) {
            return "block"
        }
        else {
            return null
        }
    };

    const handleClick = () => {
        setStateStatus(!stateStatus);
        props.onChangeStatus();
    };

    return (<tr className="player-list_row">
        <td className="first_colomn">
            {props.name}
        </td>
        <td className="age_colomn">
            {props.age}
        </td>
        <td className="gender_colomn">
            <MyImage
                name={imageName}
                width={24}
                height={24}
            />
        </td>
        <td className="status_colomn">
            <Chip
                className={playerStatus}
                text={statusText}
            />
        </td>
        <td className="tipic_colomn">
            {props.creatingDate}
        </td>
        <td className="tipic_colomn">
            {props.changedDate}
        </td>
        <td className="last_colomn">
            <MyButton
                imageName={setBlock()}
                className="button background-lock unlock"
                buttonText={statusAction}
                onClickFunction={handleClick}
            />
        </td>
    </tr>);
};

export default PlayerListRow;