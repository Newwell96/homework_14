import React from 'react';
import './xo-panel.css';
import MyGameInfo from "./gameInfo/game-info";
import MyChat from "./gameChat/chat";
import MyBoard from "./gameBoard/game-board";
import gameStore from "../../store/GameStore";
import {GameContext} from "../../store/GameStore";

function XOPanel() {

    return (
        <GameContext.Provider value={gameStore}>
        <div id="main-container">
            <MyGameInfo/>
            <MyBoard/>
            <MyChat/>
        </div>
            </GameContext.Provider>
    )
}

export default XOPanel;
