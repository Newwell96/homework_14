import React from 'react';
import MyImage from "../../../../../UI components/image/image";

const GameInfoMove = (
    {
        side,
        name
    }
) => {
    let pic;
    if (side === "X") {
        pic = "miniX";
    }
    else if (side === "O") {
        pic = "miniZero";
    }
    return (
        <div id="game-step">
            Ходит
            <MyImage
                name={pic}
                width={24}
                height={24}
            />
            {name}
        </div>
    );
};

export default GameInfoMove;