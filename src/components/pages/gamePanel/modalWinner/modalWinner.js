import React, {useContext} from 'react';
import './modalWinner.css';
import MyImage from "../../../UI components/image/image";
import MyButton from "../../../UI components/button/button";
import {observer} from "mobx-react";
import {GameContext} from "../../../store/GameStore";

const MyModalWinner = () => {
    const gameStore = useContext(GameContext);

    function refreshPage() {
        window.location.reload();
    }
    function clearLocalStorage() {
        const userData = localStorage.getItem('userData');
        localStorage.clear();
        localStorage.setItem('userData', userData);
    }

    return (
        <div className={gameStore.modalActive ? "modal active": "modal"}>
            <div className={gameStore.modalActive ? "modal-winner show": "modal-winner"}>
                <MyImage
                    name="trophy"
                    width={132}
                    height={165.36}
                />
                <div className="winner-text">
                    {gameStore.winnerName} победил!
                </div>
                <MyButton
                    className="button background-default-green"
                    onClickFunction ={
                    () => {
                        clearLocalStorage();
                        refreshPage();
                    }}
                    buttonText="Новая игра"
                />
                <MyButton
                    className="button back-menu"
                    onClickFunction = {
                        () => {
                            gameStore.setModalActive();
                        }
                    }
                    buttonText="Закрыть окно"
                />
            </div>
        </div>
    );
};

export default observer (MyModalWinner);
