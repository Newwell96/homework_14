import React, {useContext} from "react";
import './game-info.css';
import MyPlayerInfo from "./components/game-info-player";
import {observer} from "mobx-react";
import {GameContext} from "../../../store/GameStore";

function MyGameInfo() {
    const gameStore = useContext(GameContext);
    return (<div id="subject-list">
            <h1>Игроки</h1>
            <div className="container">
                {gameStore.users ? (
                    <>
                        <MyPlayerInfo
                            pic={gameStore.playerO?.side}
                            playerName={gameStore.playerO?.surName + " " + gameStore.playerO?.firstName + " " + gameStore.playerO?.secondName}
                            percentWin={gameStore.playerO?.percentWin + " побед"}
                        />
                        <MyPlayerInfo
                            pic={gameStore.playerX?.side}
                            playerName={gameStore.playerX?.surName + " " + gameStore.playerX?.firstName + " " + gameStore.playerX?.secondName}
                            percentWin={gameStore.playerX?.percentWin + " побед"}
                        />
                    </>) : null}


            </div>
        </div>)
}

export default observer(MyGameInfo);
