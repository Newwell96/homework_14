import React from "react";
import PropTypes from "prop-types";
import './input.css';
import classNames from "classnames";

function MyInput ({
    label,
    containerClassName,
    labelClassName,
    className,
    inputName,
    error,
    errorStyle,
    ...attrs
}) {
    const classes = classNames (
      "input",
      className,
        {error},
    );
    return (
        <div className={containerClassName}>
            {label &&
                <label
                    className={labelClassName}
                >
                    {label}
                </label>
            }
            <input
                className={className}
                name={inputName}
                {...attrs}
            />
                {error &&
                    <span className={errorStyle} >{error}</span>
                }
        </div>
    );
}

MyInput.propTypes = {
    className:PropTypes.string,
    label: PropTypes.string,
    error: PropTypes.string,
};

MyInput.defaultProps = {
    className: "",
    label: "",
    error: "",
};

export default MyInput;