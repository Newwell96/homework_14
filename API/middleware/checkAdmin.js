const jwt = require('jsonwebtoken');
const getAccount = require("../modules/accounts_checker");

const checkAdmin = async (req, res, next) => {
    const secretKey = process.env.SECRET_KEY;
    const token = req.headers.authorization.split(' ')[1];
    try {
        const decodedToken = jwt.verify(token, secretKey);
        console.log(decodedToken);
        const login = decodedToken.login;
        const data = await getAccount(login);

        // Сравнение с БД и дальнейшие действия
        if (data.isAdmin===false) {
            console.log(data.isAdmin);
            res.status(403);
            res.send({ message: 'Access denied' });
            return;
        }
        res.status(200);
        res.send({ message: 'OK' });
        next();
    } catch (err) {
        // Обработка ошибки
        res.status(401);
        res.send({ message: 'Unauthorized' });
    }
}


module.exports = checkAdmin;

