const knex = require("../db/knex");

const getUser = function (id) {

    return knex('accounts')
        .select("*")
        .where(
            {
                id: id
            }
        )
        .first()
        .then((account) => {
            return account;
        })
        .catch((err) => {
            console.error(err);
        })
};

module.exports = getUser;