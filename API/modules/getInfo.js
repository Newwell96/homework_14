const knex = require("../db/knex");

const getInfo = async function (account) {

    try {
        const playerX = account.idPlayer1;
        const playerO = account.idPlayer2;

        const getPlayer = async (player) => {
            return knex('users').where('id', player).first();
        }

        const [playerXData, playerOData] = await Promise.all([getPlayer(playerX), getPlayer(playerO)]);

        return {"X": {
                ...playerXData,
                "side": "X",
                "percentWin": 63,
            },
            "O": {
                ...playerOData,
                "side": "O",
                "percentWin": 23,
            },
                "timeStart": account.gameBegin,
            }
        ;

    } catch (err) {
        console.error(err);
        throw new Error("Failed to get game info");
    }
};

module.exports = getInfo;
