var express = require('express');
const authMiddleware = require("../middleware/auth");
var router = express.Router();
var myGlobalArray = [];

router.use(authMiddleware)

router.get("/", function (req, res, next) {
    const index = req.query.index;
    if (myGlobalArray[index]) {
        console.log("получение данных");
        const gameField = myGlobalArray[index];
        res.status(200);
        res.send({gameField} );
        console.log("Успех!");
    }
    else {
        res.status(404);
        res.send("Данные не найдены");
    }
});

router.post("/", function (req, res, next) {
    if (req.body) {
        console.log("запись данных");
        myGlobalArray[0] = [...req.body];
        res.status(200);
        res.send("Успех!");
    }
    else{
        res.status(404);
        res.send("В теле запроса нет данных!");
    }
});

router.put("/", function (req, res, next) {
    const field = req.body;
    const index = req.query.index;
    if (myGlobalArray[index]) {
        console.log("замена данных");
        myGlobalArray[index] = field;
        res.status(200);
        res.send("Успех!");
    }
    else {
        res.status(404);
        res.send("Данные не найдены");
    }
});

router.delete("/", function (req, res, next) {
    const index = req.query.index;
    if (myGlobalArray[index]) {
        console.log("удаление данных");
        delete myGlobalArray[index];
        res.status(200);
        res.send("Успех!");
    }
    else {
        res.status(404);
        res.send("Данные не найдены");
    }
});

module.exports = router;