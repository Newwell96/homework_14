module.exports = {
  client: 'pg',
  connection: {
    host: 'localhost',
    port: process.env.DB_PORT,
    user: 'postgres',
    password: process.env.DB_PASSWORD,
    database: 'db_xoxo_dev'
  },
  pool: {
    min:0,
    max:10
  },
  migrations: {
    tableName: "knex_migrations"
  }
};