const users = require("./db_xoxo_dev_public_users.json");
const accounts = require("./db_xoxo_dev_public_accounts.json");
const games = require("./db_xoxo_dev_public_games.json");

exports.seed = function (knex) {
    return knex('users').del()
        .then(function () {
            return knex('accounts').del();
        })
        .then(function () {
            return knex('games').del();
        })
        .then(function () {
            return knex('users').insert(users);
        })
        .then(function () {
            return knex('accounts').insert(accounts);
        })
        .then(function () {
            return knex('games').insert(games);
        });
};
