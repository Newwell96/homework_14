var knexConfig = require('./knexConfig');
var knex = require('knex')(knexConfig);
// const knex = require('knex')(knexConfig[process.env.NODE_ENV])

module.exports = knex;
