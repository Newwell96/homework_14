exports.up = function(knex) {
    return knex.schema
        .createTable('users', function (table) {
            table.integer('id').primary();
            table.string('surName').notNullable();
            table.string('firstName').notNullable();
            table.string('secondName').notNullable();
            table.integer('age').notNullable();
            table.boolean('gender').notNullable();
            table.boolean('statusActive').notNullable();
            table.boolean('statusGame').notNullable();
            table.timestamp('createdAt',{ useTz: false }).notNullable();
            table.timestamp('updatedAt',{ useTz: false }).notNullable();
        })
        .createTable("accounts", function (table) {
                table.integer('idUser');
                table.foreign('idUser').references('id').inTable('users');
                table.string('login').notNullable();
                table.string('password').notNullable();
                table.boolean('isAdmin').notNullable();
            })
        .createTable("games", function (table)
            {
                table.integer('id').primary();
                table.integer('idPlayer1').notNullable();
                table.foreign('idPlayer1').references('id').inTable('users');
                table.integer('idPlayer2').notNullable();
                table.foreign('idPlayer2').references('id').inTable('users');
                table.boolean('gameResult');
                table.timestamp('gameBegin',{ useTz: false }).notNullable();
                table.timestamp('gameEnd',{ useTz: false }).nullable();
            }
        )
        .createTable("messenger", function (table)
            {
                table.integer('idGame');
                table.foreign('idGame').references('id').inTable('games');
                table.integer('idUser');
                table.foreign('idUser').references('id').inTable('users');
                table.text('message').notNullable();
                table.timestamp('createdAt',{ useTz: false }).notNullable();
            }
        );
};

exports.down = function(knex) {
    return knex.schema
        .dropTable("messenger")
        .dropTable("games")
        .dropTable("accounts")
        .dropTable("users");
};
