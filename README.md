# _Домашнее задание №14_

Наличие механизмов идентификации, авторизации и аутентификации позволяют реализовать игру между двумя игроками по сети.
Что нужно сделать:

1. Реализовать аутентификацию пользователей

   -шифровать пароль хэш-функцией Argon2id / bcrypt с добавлением соли

2. Добавить промежуточный слой, отвечающий за авторизацию, т.е. за то, может ли игрок выполнить действие или нет.
3. Реализовать возможность игры по сети - записать видео, на котором видно, что используя две разных сессии браузера и два разных аккаунта происходит партия игры.

**Реализовывать чат между игроками не надо!**

В качестве ответа необходимо отправить видео и ссылку на гитлаб. 